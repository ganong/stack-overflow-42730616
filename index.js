var initialState = {
    data: {
        firstname: '',
        lastname: '',
        password: '',
        emailaddress: '',
    },
    users: [],
};

var AccountView = React.createClass({

getInitialState: function() {
    return initialState;
},

componentDidMount: function() {
    $.ajax({
        url: "/get_accounts.json",
        async: "false",
        dataType: "json",
        sucess: function(json) {
            console.log("success!");
            console.log(json);      

        },
        error:function(x, e) {
            alert(e);
        },
        complete: function(a, data) {
            var users = JSON.parse(a.responseText);
            this.setState({
                users: users,
            });
        }.bind(this)
    });
},

handleChange: function(e) {
    var data = Object.assign({}, this.state.data);
    data[e.target.name] = e.target.value;
    this.setState({
        data: data,
    });
},

handleSave: function(e) {
    e.preventDefault();
    var users = this.state.users.slice();
    users.push(this.state.data);
    $.ajax({
        url: "/post_accounts.json",
        type: "post",
        data: this.state.data,
        success: function(data, a) {
            this.setState({
                data: Object.assign({}, initialState.data),
            });
        }.bind(this),
        error: function(e) {

        },
        complete: function(a, data) {
            this.setState({
                data: Object.assign({}, initialState.data),
                users: users,
            });
        }.bind(this)
    });
},

render: function() {
    return (
        <div className="container">
            <form method="post">    
                <dl id="form" className="row">
                    <div className="col-md-4">
                        <dt>Firstname</dt>
                        <dd>
                            <input name="firstname" type="text" value={this.state.data.firstname} onChange={this.handleChange} />
                        </dd>
                        <dt>Lastname</dt>
                        <dd>
                            <input name="lastname" type="text" value={this.state.data.lastname} onChange={this.handleChange} />
                        </dd>
                        <dt>Password</dt>
                        <dd>
                            <input name="password" type="text" value={this.state.data.password} onChange={this.handleChange} />
                        </dd>
                        <dt>Email Address</dt>
                        <dd>
                            <input name="emailaddress" type="text" value={this.state.data.emailaddress} onChange={this.handleChange} />
                        </dd>
                        <dt>Submit</dt>
                        <dd>
                            <input type="submit" value="Submit" onClick={this.handleSave} />
                        </dd>
                    </div>
                    <div className="col-md-8">
                        <table>
                            <thead>
                                <tr>
                                    <th>Firstname</th>
                                    <th>Lastname</th>
                                    <th>Password</th>
                                    <th>Email address</th>
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.users.map(function(user) {
                            return (
                                    <tr>
                                        <td>{user.firstname}</td>
                                        <td>{user.lastname}</td>
                                        <td>{user.password}</td>
                                        <td>{user.emailaddress}</td>
                                    </tr>
                                    );  
                            })}
                            </tbody>
                        </table>
                    </div>
                </dl>
            </form>
        </div>
    );
}

});




ReactDOM.render(
  <AccountView />,
  document.getElementById('root')
);
